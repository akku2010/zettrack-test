import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TtPage } from './tt';

@NgModule({
  declarations: [
    TtPage,
  ],
  imports: [
    IonicPageModule.forChild(TtPage),
  ],
})
export class TtPageModule {}
