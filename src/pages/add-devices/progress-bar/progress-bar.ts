import {Component, Input} from '@angular/core';


@Component({
  selector: 'progress-bar',
  templateUrl: 'progress-bar.html',
  styles: [
      `.progress-outer {
        width: 100%;
        margin: 0px;
        padding: 0px;
        text-align: center;
        background-color: #f4f4f4;
        border: 1px solid #dcdcdc;
        color: #fff;
        border-radius: 0px;
      }
    
      .progress-inner {
        min-width: 15%;
        white-space: nowrap;
        overflow: hidden;
        padding: 2px;
        border-radius: 0px;
        background-color: #488aff;
      }`
  ]
})
export class ProgressBarComponent {

  @Input('progress') progress;


  public constructor() {

  }

}